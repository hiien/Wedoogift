package test.java.user;

import main.java.deposittypeimpl.DepositGift;
import main.java.deposittypeimpl.DepositMeal;
import main.java.user.User;
import org.junit.Assert;
import org.junit.BeforeClass;
import org.junit.Test;

public class UserTest {
    static User classToTest;

    @BeforeClass
    public static void setUp() {
        classToTest = new User("UserTest");
    }
    @Test
    public void TestGetUsername() {
        Assert.assertEquals("UserTest", classToTest.getUserName());
    }

    @Test
    public void TestSetDepositMeal() {
        classToTest.setDeposit(new DepositMeal(), 100);
        Assert.assertEquals(100, classToTest.getDepositTypeWithKey(DepositMeal.class).getDepositAmount());
    }

    @Test
    public void TestGetDepositMeal() {
        classToTest.setDeposit(new DepositMeal(), 100);
        Assert.assertEquals(DepositMeal.class, classToTest.getDepositTypeWithKey(DepositMeal.class).getClass());
    }

    @Test
    public void TestSetDepositGift() {
        classToTest.setDeposit(new DepositGift(), 50);
        Assert.assertEquals(50, classToTest.getDepositTypeWithKey(DepositGift.class).getDepositAmount());
    }

    @Test
    public void TestGetDepositGift() {
        classToTest.setDeposit(new DepositGift(), 50);
        Assert.assertEquals(DepositGift.class, classToTest.getDepositTypeWithKey(DepositGift.class).getClass());
    }

}
