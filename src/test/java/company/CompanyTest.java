package test.java.company;

import main.java.company.Company;
import main.java.deposittypeimpl.DepositGift;
import main.java.deposittypeimpl.DepositMeal;
import main.java.user.User;
import org.junit.Assert;
import org.junit.BeforeClass;
import org.junit.Test;

public class CompanyTest {
    static Company classToTest;
    static User userTest;

    @BeforeClass
    public static void setUp() {
        classToTest = new Company("CompanyTest", 500);
        userTest = new User("UserTest");
    }

    @Test
    public void TestGetCompanyName() {
        Assert.assertEquals("CompanyTest", classToTest.getCompanyName());
    }

    @Test
    public void TestGiveMealDeposit() {
        classToTest.giveMealDeposit(userTest, 100);
        Assert.assertEquals(100, userTest.getDepositTypeWithKey(DepositMeal.class).getDepositAmount());
    }

    @Test
    public void TestGiveGiftDeposit() {
        classToTest.giveGiftDeposit(userTest, 50);
        Assert.assertEquals(50, userTest.getDepositTypeWithKey(DepositGift.class).getDepositAmount());
    }

    @Test
    public void TestBudgetIsUpdated() {
        classToTest = new Company("CompanyTest", 500);
        Assert.assertEquals(500, classToTest.getBudget());
        classToTest.giveGiftDeposit(userTest, 100);
        Assert.assertEquals(400, classToTest.getBudget());
    }

    @Test
    public void TestBudgetIsUpdated2() {
        classToTest = new Company("CompanyTest", 500);
        Assert.assertEquals(500, classToTest.getBudget());
        classToTest.giveMealDeposit(userTest, 200);
        Assert.assertEquals(300, classToTest.getBudget());
    }

    @Test
    public void TestBudgetIsNotUpdated() {
        classToTest = new Company("CompanyTest", 500);
        Assert.assertEquals(500, classToTest.getBudget());
        classToTest.giveMealDeposit(userTest, 600);
        Assert.assertEquals(500, classToTest.getBudget());
    }

    @Test
    public void TestBudgetIsNotUpdated2() {
        classToTest = new Company("CompanyTest", 500);
        Assert.assertEquals(500, classToTest.getBudget());
        classToTest.giveGiftDeposit(userTest, 600);
        Assert.assertEquals(500, classToTest.getBudget());
    }
}
