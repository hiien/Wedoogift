package test.java.deposittypeimpl;

import main.java.deposittypeimpl.DepositGift;
import org.junit.Assert;
import org.junit.BeforeClass;
import org.junit.Test;

import java.time.LocalDate;
import java.time.format.DateTimeFormatter;

public class DepositGiftTest {

    static DepositGift classToTest;

    @BeforeClass
    public static void setUp() {
        classToTest = new DepositGift();
        classToTest.deposit(100);
    }

    @Test
    public void TestDeposit() {
        Assert.assertEquals(100, classToTest.getDepositAmount());
    }

    @Test
    public void TestDespositExpirationDate() {
        DateTimeFormatter dtf = DateTimeFormatter.ofPattern("MM/dd/uuuu");
        LocalDate depositDate = LocalDate.of(1999, 4, 23);
        depositDate = depositDate.plusYears(1);
        String expirationDate = dtf.format(depositDate);

        Assert.assertEquals("04/23/2000", expirationDate);
    }

    @Test
    public void TestGetDepositAmount() {
        classToTest.deposit(100);
        Assert.assertEquals(100, classToTest.getDepositAmount());
    }

    @Test
    public void TestUpdateDepositAmount() {
        Assert.assertEquals(100, classToTest.getDepositAmount());
        classToTest.updateDepositAmount(50);
        Assert.assertEquals(50, classToTest.getDepositAmount());
    }
}
