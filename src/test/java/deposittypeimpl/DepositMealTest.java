package test.java.deposittypeimpl;

import main.java.deposittypeimpl.DepositGift;
import org.junit.Assert;
import org.junit.BeforeClass;
import org.junit.Test;

import java.time.LocalDate;
import java.time.format.DateTimeFormatter;

public class DepositMealTest {

    static DepositGift classToTest;

    @BeforeClass
    public static void setUp() {
        classToTest = new DepositGift();
        classToTest.deposit(100);
    }

    @Test
    public void TestDeposit() {
        Assert.assertEquals(100, classToTest.getDepositAmount());
    }

    @Test
    public void TestDespositExpirationDate() {
        DateTimeFormatter dtf = DateTimeFormatter.ofPattern("MM/dd/uuuu");
        LocalDate depositDate = LocalDate.of(1993, 4, 23);
        depositDate = depositDate.plusYears(1);
        depositDate = depositDate.withMonth(2);
        depositDate = depositDate
                .withDayOfMonth(depositDate.getMonth().length(depositDate.isLeapYear()));
        String expirationDate = dtf.format(depositDate);

        Assert.assertEquals("02/28/1994", expirationDate);
    }

    @Test
    public void TestGetDepositAmount() {
        classToTest.deposit(100);
        Assert.assertEquals(100, classToTest.getDepositAmount());
    }

    @Test
    public void TestUpdateDepositAmount() {
        Assert.assertEquals(100, classToTest.getDepositAmount());
        classToTest.updateDepositAmount(50);
        Assert.assertEquals(50, classToTest.getDepositAmount());
    }
}
