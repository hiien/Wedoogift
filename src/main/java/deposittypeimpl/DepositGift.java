package main.java.deposittypeimpl;

import main.java.deposittype.AbstractDepositType;

import java.time.LocalDate;
import java.time.format.DateTimeFormatter;

public class DepositGift extends AbstractDepositType {

    private static int depositGiftLife = 1;

    @Override
    protected void setDepositExpirationDate() {
        DateTimeFormatter dtf = DateTimeFormatter.ofPattern("MM/dd/uuuu");
        LocalDate lcExpirationDate = LocalDate.now();
        lcExpirationDate = lcExpirationDate.plusYears(depositGiftLife);
        this.expirationDate = dtf.format(lcExpirationDate);
    }
}
