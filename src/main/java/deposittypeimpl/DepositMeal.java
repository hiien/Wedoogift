package main.java.deposittypeimpl;

import main.java.deposittype.AbstractDepositType;

import java.time.LocalDate;
import java.time.format.DateTimeFormatter;

public class DepositMeal extends AbstractDepositType {

    private static int depositYearMealLife = 1;
    private static int depositMonthMealLife = 2;

    @Override
    protected void setDepositExpirationDate() {
        DateTimeFormatter dtf = DateTimeFormatter.ofPattern("MM/dd/uuuu");
        LocalDate lcExpirationDate = LocalDate.now();
        lcExpirationDate = lcExpirationDate.plusYears(depositYearMealLife);
        lcExpirationDate = lcExpirationDate.withMonth(depositMonthMealLife);
        lcExpirationDate = lcExpirationDate.withDayOfMonth(lcExpirationDate.getMonth().length(lcExpirationDate.isLeapYear()));
        this.expirationDate = dtf.format(lcExpirationDate);
    }
}
