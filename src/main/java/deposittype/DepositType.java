package main.java.deposittype;

public interface DepositType {

    void deposit(int amount);

    String getDepositExpirationDate();

    int getDepositAmount();

    void updateDepositAmount(int amount);
}
