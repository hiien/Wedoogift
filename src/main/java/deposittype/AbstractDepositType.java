package main.java.deposittype;

public abstract class AbstractDepositType implements DepositType {

    protected String expirationDate;
    private int amount;

    @Override
    public void deposit(int amount) {
        this.amount = amount;
        setDepositExpirationDate();
    }

    protected abstract void setDepositExpirationDate();

    @Override
    public String getDepositExpirationDate() {
        return this.expirationDate;
    }

    @Override
    public int getDepositAmount() {
        return this.amount;
    }

    @Override
    public void updateDepositAmount(int usedAmount) {
        int updatedAmount = this.amount - usedAmount;
        this.amount = updatedAmount < 0 ? 0 : updatedAmount;
    }
}
