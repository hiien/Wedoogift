package main.java.company;

import main.java.deposittype.DepositType;
import main.java.deposittypeimpl.DepositGift;
import main.java.deposittypeimpl.DepositMeal;
import main.java.user.User;

public class Company {
    private String companyName;
    private int budget;

    public void giveMealDeposit(User user, int amount) {
        if (isBudgetUpdatable(this.budget, amount)) {
            updateBudget(user, new DepositMeal(), amount, this.budget - amount);
        } else {
            System.out.println(this.companyName + " insufficient budget in order to give meal deposit to " + user.getUserName());
        }
    }

    public void giveGiftDeposit(User user, int amount) {
        if (isBudgetUpdatable(this.budget, amount)) {
            updateBudget(user, new DepositGift(), amount, this.budget - amount);
        } else {
            System.out.println(this.companyName + " insufficient budget in order to give gift deposit to " + user.getUserName());
        }
    }

    private boolean isBudgetUpdatable(int companyBudget, int amount) {
        return companyBudget - amount < 0 ? false : true;
    }

    private void updateBudget(User user, DepositType depositType, int amount, int updatedBudget) {
        user.setDeposit(depositType, amount);
        setBudget(updatedBudget);
    }

    public Company(String companyName, int budget) {
        this.companyName = companyName;
        this.budget = budget;
    }

    public String getCompanyName() {
        return companyName;
    }

    public int getBudget() {
        return budget;
    }

    public void setBudget(int budget) {
        this.budget = budget;
    }
}
