package main.java.user;

import main.java.deposittype.DepositType;

import java.util.HashMap;

public class User {

    private String userName;

    /**
     * DepositType can be replaced by List<DepositType> if multiple deposit of same type
     */
    private HashMap<Class, DepositType> deposits = new HashMap<>();

    public User(String userName) {
        this.userName = userName;
    }

    public String getUserName() {
        return this.userName;
    }

    public void setDeposit(DepositType depositType, int amount) {
        deposits.put(depositType.getClass(), depositType);
        depositType.deposit(amount);
    }

    public DepositType getDepositTypeWithKey(Class<?> depositType) {
        return deposits.get(depositType);
    }

}
