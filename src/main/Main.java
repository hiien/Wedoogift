package main;

import main.java.company.Company;
import main.java.deposittypeimpl.DepositGift;
import main.java.deposittypeimpl.DepositMeal;
import main.java.user.User;

public class Main {
    public static void main(String[] args) {

        Company c1 = new Company("Tesla", 1000);
        User u1 = new User("John");
        c1.giveGiftDeposit(u1, 100);

        Company c2 = new Company("Apple", 500);
        User u2 = new User("Jessica");
        c2.giveMealDeposit(u2, 50);

        /*** FIRST EXAMPLE GIFT => OK ***/
        System.out.println("=== First example ===");
        System.out.println("Company " + c1.getCompanyName() + " gives gift card to employee " + u1.getUserName()
            + " " + u1.getDepositTypeWithKey(DepositGift.class).getDepositAmount() + "€ which will expire on "
            + u1.getDepositTypeWithKey(DepositGift.class).getDepositExpirationDate()
        );
        u1.getDepositTypeWithKey(DepositGift.class).updateDepositAmount(42);
        System.out.println("John uses 42€, his current balance is now : " + u1.getDepositTypeWithKey(DepositGift.class).getDepositAmount() + "€");

        System.out.println();

        /*** SECOND EXAMPLE MEAL => OK ***/
        System.out.println("=== Second Example ===");
        System.out.println("Company " + c2.getCompanyName() + " gives meal card to employee " + u2.getUserName()
            + " " + u2.getDepositTypeWithKey(DepositMeal.class).getDepositAmount() + "€ which will expire on "
            + u2.getDepositTypeWithKey(DepositMeal.class).getDepositExpirationDate()
        );
        u2.getDepositTypeWithKey(DepositMeal.class).updateDepositAmount(12);
        System.out.println("Jessica uses 12€, her current balance is now : " + u2.getDepositTypeWithKey(DepositMeal.class).getDepositAmount() + "€");

        System.out.println();

        /*** THIRD EXAMPLE MEAL => NOK ***/
        System.out.println("=== Third Example ===");
        System.out.println("After giving to Jessica 50€, Apple budget is now : " + c2.getBudget());
        System.out.println("Let's try give another 600€ to Jessica !");
        c2.giveMealDeposit(u2, 600);
    }
}